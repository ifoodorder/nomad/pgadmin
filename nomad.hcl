job "pgadmin" {
	datacenters = ["dc1"]
	type        = "service"

	group "pgadmin" {
		network {
			port "http" {
				static = 5434
				to = 5434
			}
		}
		service {
			name = "pgadmin"
			tags = ["management"]
			port = "5434"
		}
		task "pgadmin" {
			vault {
				policies = ["pgadmin"]
			}
			template {
				data =<<EOH
{{ with $ip_address := (env "NOMAD_HOST_IP_http") }}
{{ with secret "pki_int/issue/cert" "role_name=pgadmin" "common_name=pgadmin.service.consul" "ttl=24h" "alt_names=_pgadmin._tcp.service.consul,localhost" (printf "ip_sans=127.0.0.1,%s" $ip_address) }}
{{ .Data.certificate }}
{{ end }}{{ end }}
EOH
				destination = "local/cert.pem"
			}
			template {
				data =<<EOH
{{ with $ip_address := (env "NOMAD_HOST_IP_http") }}
{{ with secret "pki_int/issue/cert" "role_name=pgadmin" "common_name=pgadmin.service.consul" "ttl=24h" "alt_names=_pgadmin._tcp.service.consul,localhost" (printf "ip_sans=127.0.0.1,%s" $ip_address) }}
{{ .Data.private_key }}
{{ end }}{{ end }}
EOH
				destination = "secrets/key.pem"
			}
			template {
				data        = <<EOH
				PGADMIN_LISTEN_PORT=5434
				PGADMIN_DEFAULT_EMAIL=test@test.test
				PGADMIN_DEFAULT_PASSWORD=test
				PGADMIN_ENABLE_TLS=true
				EOH
				destination = "secrets/file.env"
				env         = true
			}
			template {
				data =<<EOH
{{ with secret "pki_int/cert/ca_chain" }}{{ .Data.certificate }}
{{ end }}
				EOH
				destination = "local/ca_chain.pem"
			}
			template {
				data = <<EOH
# -*- coding: utf-8 -*-

APP_NAME = 'iFoodOrder pgAdmin'
WTF_CSRF_HEADERS = ['X-CSRF-Token']
DEFAULT_SERVER_PORT=5434
ALLOWED_HOSTS = []
PROXY_X_FOR_COUNT = 1
PROXY_X_PROTO_COUNT = 1
PROXY_X_HOST_COUNT = 1
PROXY_X_PORT_COUNT = 2
PROXY_X_PREFIX_COUNT = 0
MAIL_SERVER = 'smtp.zoho.eu'
MAIL_PORT = 465
MAIL_USE_SSL = True
MAIL_USE_TLS = False
MAIL_USERNAME = 'ifoodorder@ifoodorder.com'
MAIL_PASSWORD = '{{ with secret "secret/pgadmin/email" }}{{ .Data.password }}{{ end }}'
MAIL_DEBUG = False
SECURITY_EMAIL_SENDER = 'ifoodorder@ifoodorder.com'
AUTHENTICATION_SOURCES = ['internal', 'ldap']
LDAP_AUTO_CREATE_USER = True
LDAP_CONNECTION_TIMEOUT = 10
LDAP_SERVER_URI = 'ldaps://freeipa-ldap.service.consul:636'
LDAP_USERNAME_ATTRIBUTE = 'uid'
LDAP_BIND_USER = 'uid=pgadmin,cn=sysaccounts,cn=etc,dc=ifoodorder,dc=com'
LDAP_BIND_PASSWORD = '{{ with secret "secret/pgadmin/ldap" }}{{ .Data.password }}{{ end }}'
LDAP_ANONYMOUS_BIND = False
LDAP_BASE_DN = 'cn=users,cn=accounts,dc=ifoodorder,dc=com'
LDAP_SEARCH_BASE_DN = 'cn=users,cn=accounts,dc=ifoodorder,dc=com'
LDAP_SEARCH_FILTER = '(cn=*)'
LDAP_SEARCH_SCOPE = 'SUBTREE'
LDAP_USE_STARTTLS = True
LDAP_CA_CERT_FILE = '/pgadmin4/ca_chain.pem'
LDAP_CERT_FILE = ''
LDAP_KEY_FILE = ''
EOH
				destination = "secrets/config_local.py"
			}

			driver = "docker"
			config {
				image = "dpage/pgadmin4:5.6"
				volumes = [
					"secrets/config_local.py:/pgadmin4/config_local.py",
					"local/ca_chain.pem:/pgadmin4/ca_chain.pem",
					"local/cert.pem:/certs/server.cert",
					"secrets/key.pem:/certs/server.key"
				]
				ports = ["http"]
			}
			volume_mount {
				volume      = "data"
				destination = "/var/lib/pgadmin"
			}
		}
		volume "data" {
			type      = "host"
			source    = "pgadmin"
			read_only = false
		}
	}
}